import React from 'react';
import { useSelector} from "react-redux";

import MainFuncs from '../../components/MainFuncs/MainFuncs'

const HomePage = () => {
    const data = useSelector((state)=> state.card.card)
    
        return (
            <>
            {data && <MainFuncs data={data} />}
    
            </>
        )
    }
export default HomePage;