import React from "react";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal"
import MainRoutes from "./Routes/Routes";
import {useDispatch} from "react-redux";
import {getData} from "./store/actionAC/cardAC";


import { useEffect, useState } from "react";




const App = () => { 
const dispatch = useDispatch()
useEffect(()=>dispatch(getData()),[])

  return (
    <>
      <Header />
      <Modal/>
      <MainRoutes/>
      
    </>
  );
};
export default App;
