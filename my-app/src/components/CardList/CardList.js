import React from "react";
import {useSelector} from "react-redux";

import styles from "./CardList.module.scss";
import Card from "../Card/Card";


const CardList = (props) => {
  // const { items, addToCart, setFavorite } = props;
  const isLoading = useSelector(state => state.card.isLoading)
  const { data } = props;

  return (
    <section className={styles.root}>
      <div className={styles.container}>
      {!isLoading &&
          data.map((elem) => (<Card key={elem.article}  id={elem.article} url={elem.url} price={elem.price} name={elem.name}/>
          ))}
      </div>
    </section>
  );
};

export default CardList;
