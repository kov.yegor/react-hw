import React from 'react';
import { useSelector } from 'react-redux';
import styles from './FavItemContainer.module.scss'
import FavItem from '../FavItem/FavItem'

const FavItemContainer = () => {
    const favData = useSelector((state)=>state.fav.card)

    return (
        <section className={styles.root}>
         
            <div className={styles.container}>
            {!favData ? <p>There is no fav items</p> : favData.map((elem)=><FavItem count={elem.count} name={elem.name} id={elem.aricle} img={elem.url} key={elem.article} />)}
            </div>
        </section>
    )
}

export default FavItemContainer;