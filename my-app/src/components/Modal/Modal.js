import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button'
import {useDispatch, useSelector} from "react-redux";
import {setIsOpenModal} from "../../store/actionAC/modalAC";
import {deleteCart} from "../../store/actionAC/cartAC";

const Modal= () => {
    const isOpen = useSelector(state => state.modal.isOpen)
    const configModal = useSelector(state => state.modal.configModal)
const dispatch = useDispatch()
    if (!isOpen) return null;

    const closeModal = ()=>{
        dispatch(setIsOpenModal(false))
    }
    const handleYes = () =>{
        dispatch(deleteCart(configModal.id))
        dispatch(setIsOpenModal(false))
    }
    return (
         <div className={styles.root}>
             <div onClick={closeModal} className={styles.background} />
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button handleClick={closeModal} className={styles.btn} text='X' ></Button>
                 </div>
                 <h2>Go Delete {configModal && configModal.name} ?</h2>
                 <div className={styles.buttonContainer}>
                     <Button handleClick={handleYes} text='Contine'></Button>
                     <Button handleClick={closeModal} text='Cancel'></Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
