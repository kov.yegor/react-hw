import React from "react";
import ItemsContainer from "../CardList/CardList";
import styles from "./MainFuncs.module.scss";

const MainFuncs = (props) => {
  // const { items, addToCart, openModal, closeModal, isOpenFirst, setFavorite } = props;
  const {data,item} = props
  return (
    <div className={styles.mainFunc}>
      {data &&
      <ItemsContainer data={data} />
      }

    </div>
  );
};

export default MainFuncs;
