import React, { useCallback } from 'react';
import styles from './FavItem.module.scss';
import Button from '../Button/Button'
import {useDispatch} from "react-redux";


import {deleteFav} from "../../store/actionAC/favouriteAC";
const FavItem = (props) => {
    const dispatch = useDispatch()
    const {price, name,img,id } = props



const favouriteDelete = ()=>{
    dispatch(deleteFav(id))

}
    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={name}/>
                </div>
                <span className={styles.title}>{name}</span>
                <span className={styles.title}>{price}</span>
                <Button text='Delete' handleClick={favouriteDelete}/>
            </div>
        </div>
            </>
    )
}

export default FavItem;