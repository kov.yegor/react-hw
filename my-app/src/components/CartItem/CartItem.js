import React from 'react';
import { useDispatch } from 'react-redux';
import {setModal, setIsOpenModal} from "../../store/actionAC/modalAC";

import styles from './CartItem.module.scss';
import Button from '../Button/Button'

const CartItem = (props) => {
    const {count,name , img,text, article} = props;
    const dispatch = useDispatch()
    const handleDelete = () =>{
        dispatch(setIsOpenModal(true))
        dispatch(setModal({name, article}))
    }
    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.cartWrap}>
                <img className={styles.av} src={img} alt={text}/>
                </div>
                <span className={styles.title}>{text}</span>
            </div>


            <span className={styles.count}>{count}</span>

            <div className={styles.buttonCont}>
       
                <Button text ="Delete" handleClick={handleDelete}></Button>
            </div>

        </div>
            </>
    )
    }

export default CartItem;