import React from 'react';
import { useSelector } from 'react-redux';
import styles from './CartContainer.module.scss'
import CartItem from '../../components/CartItem/CartItem'

const CartContainer = () => {
    const cartData = useSelector((state)=>state.cart.card)

  return (
    <section className={styles.root}>
     
        <div className={styles.container}>
        {cartData && 
        cartData.map((elem)=>
        <CartItem  count={elem.count} text={elem.name} aricle={elem.article} img={elem.url} key={elem.article}  />)}
        </div>
    </section>
)



}

export default CartContainer;