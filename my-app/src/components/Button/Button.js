import React from 'react'
import styles from './Button.module.scss'

class Button extends React.Component{



    
    render() {
const { text, handleClick} = this.props

    return (
        <>
        <button className={styles.button}  onClick={handleClick}>{text}</button>
    </>)
}
}   
export default Button;