import React from "react";
import styles from "./Card.module.scss";
import Button from "../Button/Button";
import { addCart} from "../../store/actionAC/cartAC"
import {addFav} from "../../store/actionAC/favouriteAC"
import { ReactComponent as StarIcon } from "../../assets/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/star-remove.svg";
import {useDispatch} from "react-redux";


const   Card = (props) => {
  const dispatch = useDispatch()
  const { name, price, article, url} = props;


  return (
    <div className={styles.root}>
      <div className={styles.favourites} onClick={() =>
      {dispatch(addFav({name, url, article}))}}>
        <StarIcon />
      </div>
      <p>{name}</p>
      <span>{price}</span>
      <img src={url} alt={name} />
     
      <Button handleClick={()=> dispatch(
        addCart({name, url, article}))} 
        text="Add to cart" />
    </div>
  );
};

export default Card;
