import {IS_OPEN_MODAL,SET_MODAL} from  "../actions/modalActions";

const initialValues = {
    isOpenModal: false,
    configs: null,
  };
 
const modalReducer =(state =initialValues,action)=>{
    // console.log(state);
    switch (action.type) {

        case IS_OPEN_MODAL: {
          return { ...state, isOpenModal: action.payload };
        }

        case SET_MODAL: {
          return { ...state, configs: action.payload };
        }

        default:
          return state;
      }
}
export default modalReducer;