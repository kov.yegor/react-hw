
import { combineReducers } from "redux";
import modalReducer from './modalReducer'
import favouriteReducer from './favouriteReducer'

import cartReducer from './cartReducer'

import cardReducer from './cardReducer'




const appReducer = combineReducers({
    card:cardReducer,
    modal:modalReducer,
    fav:favouriteReducer,   
    cart:cartReducer,

})

export default appReducer;