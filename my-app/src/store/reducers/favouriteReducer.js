import { getItemFromLS, setItemToLS } from "../../utils/localStorage";
import { ADD_FAVOURITE, DELETE_FAVOURITE } from "../actions/favouriteActions";
const initialValues = {
    card: getItemFromLS("fav")
     ? getItemFromLS("fav").card
     : [],
     //   console.log();
  }; 

const favouriteReducer = (state = initialValues, action)=>{
    switch (action.type) {
        case ADD_FAVOURITE: {
          const { name, url, article } = action.payload;
          const index = state.card.findIndex(
            (item) => item.article === action.payload.article
          );
          if (index === -1) {
            const setItemState = {
              ...state,
              card: [...state.card, { name, url, article }],
            };

            setItemToLS("fav", setItemState);
            const newFavData = [...state.card];
            newFavData.splice(index, 1);
            setItemToLS("fav", setItemState);
            return setItemState;
          }
    
          const newFavData = [...state.card];
          newFavData.splice(index, 1);
          setItemToLS("fav", { ...state, card: newFavData });
          return { ...state, card: newFavData };
        }


        case DELETE_FAVOURITE: {
          const index = state.card.findIndex((item) =>
           item.article === action.payload);
          if (index === -1) {
            return state;
          }
          const newFavData = [...state.card];
          newFavData.splice(index, 1);
    
          setItemToLS("fav", { ...state, card: newFavData });
          return { ...state, card: newFavData };
        }
        default:
          return state;
      }
}
export default favouriteReducer