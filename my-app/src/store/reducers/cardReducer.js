import {LOAD_CARDS,GET_DATA,DEL_DATA} from "../actions/cardActions";


const initialValues = {
    card: [],
    isLoading: false,
  };

  const cardReducer = (state = initialValues, action) => {
    switch (action?.type) {
      case GET_DATA: {
        return { ...state, card: action.payload };
      }
      case LOAD_CARDS: {
        return { ...state, isLoading: action.payload };
      }
      case DEL_DATA: {
        return { ...state, card: [] };
      }
      
      default:
        return state;
    }
  };
  
  export default cardReducer;
  