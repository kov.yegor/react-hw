import {ADD_IN_CART,DELETE_FROM_CART} from "../actions/cartActions"
import { setItemToLS,getItemFromLS  } from "../../utils/localStorage";
getItemFromLS("cart") 

const initialValues = {
    card: getItemFromLS("cart") 
    ? getItemFromLS("cart").card
    : [],
  };

//   const initialValues = {
//    if(cards:getItemFromLS("cart") ){
//     getItemFromLS("cart").cards
//    }else {
//        []
//    }   
//   };


const cartReducer = (state =initialValues ,action)=>{
    switch (action.type) {
        case ADD_IN_CART: {
          const { name, url, article } = action.payload;
    
          const index = state.card.findIndex((item) => item.article === action.payload.article);
          console.log(index);
          if (index === -1) {
            const newState = {
              ...state,
              card: [...state.card, { name, url, article, count: 1 }],
            };
            
    console.log(newState);
            setItemToLS("cart", newState);
            return newState;
          }
          
    
          const newCartData = [...state.card];
          newCartData[index].count += 1;
    
          setItemToLS("cart", { ...state, card: newCartData });
          return { ...state, card: newCartData };
        }
        case DELETE_FROM_CART: {
          const index = state.card.findIndex((item) => item.article === action.payload);
          console.log(index);
          const newCartData = [...state.card];
          newCartData.splice(index, 1);
    
          setItemToLS("cart", { ...state, card: newCartData });
          return { ...state, card: newCartData };
        }
       
        default:
          return state;
      }
}

export default cartReducer