import {IS_OPEN_MODAL, SET_MODAL} from "../actions/modalActions";

export const setIsOpenModal = (value) =>{
    return {type: IS_OPEN_MODAL, payload: value}
}

export const setModal = (value) =>{
    return {type: SET_MODAL, payload: value}
}