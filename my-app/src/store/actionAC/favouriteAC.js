import {ADD_FAVOURITE, DELETE_FAVOURITE} from "../actions/favouriteActions";

export const addFav = (data)=> {
    return {type: ADD_FAVOURITE, payload: data}
}

export const deleteFav = (id) =>{
    return {type: DELETE_FAVOURITE, payload: id}
}
