import {GET_DATA, DEL_DATA, LOAD_CARDS} from "../actions/cardActions";

export const getData= () => async (dispatch) =>{
    dispatch(isLoading(true));
    const result= await fetch('./store.json').then(res => res.json())
    dispatch(isLoading(false));
    dispatch({
         type:GET_DATA,
         payload:result
        })
}

export const isLoading = (value) => ({type: LOAD_CARDS, payload: value})

export const deleteCardAC = () => ({type: DEL_DATA})