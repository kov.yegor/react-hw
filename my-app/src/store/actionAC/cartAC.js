import {DELETE_FROM_CART,ADD_IN_CART} from '../actions/cartActions';

export const addCart = (data)=> {
    return {type: ADD_IN_CART, payload: data}
}
export const deleteCart = (id) =>{
    return {type: DELETE_FROM_CART, payload: id}
}
